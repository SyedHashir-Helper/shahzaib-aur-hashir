class challanDetails:    
    fisrtparty = []
    secondParty = []
    deedDetails = object
    def __init__(self,district,tehsil,paperType,deedName,purposeReg,agent):
        self.district = district
        self.tehsil = tehsil
        self.paperType = paperType
        self.deedName = deedName
        self.purposeReg = purposeReg
        self.agent = agent
        
class Agent:
    def __init__(self,name,cnic,contact,email):
        self.name = name
        self.cnic = cnic
        self.contact = contact
        self.email = email
        
class partyInformation(Agent):
    def __init__(self, name, cnic, contact, email,relation,relationName,address):
        super().__init__(name, cnic, contact, email)
        self.relation = relation
        self.relationName = relationName
        self.address = address

class deedDetails:
    def __init__(self,propertyAddress,amount) -> None:
        self.propertyAddress = propertyAddress
        self.amount = amount
